//= Functions & Modules
// Others
import path from 'path';
import { exec } from 'child_process';

//= Structures & Data
// Others
import { ExecOptions } from 'child_process';

const RootDir = path.join(__dirname);

async function customExec(name: string, execCommand: string, options: ExecOptions) {
    const childProcess = exec(execCommand, options);

    return new Promise((resolve, reject) => {
        childProcess.on('error', (error) => {
            console.error(`[${name}:error]${error.toString()}`);
        });

        childProcess.on('exit', (code, signal) => {
            if (code !== 0) {
                console.error(`[${name}:exit]Done, but with errors: code ${code}, signal ${signal}`);
                reject({ code, signal });
            } else {
                console.log('[buildPackage:tsc:exit]Done with code 0');
                resolve(true);
            }
        });

        childProcess.stdout.on('data', (data) => {
            process.stdout.write(`[${name}]${data.toString()}`);
        });

        childProcess.stderr.on('data', (data) => {
            process.stderr.write(`[${name}]${data.toString()}`);
        });
    });
}

async function run() {
    process.env.NODE_ENV = 'production';

    try {
        await customExec('tsc build', `npx tsc --build`, { cwd: RootDir });

        await customExec(
            'docker',
            `sudo docker build -t registry.softprovider.ro/mongodb-backup/mongodb-backup-aws-s3-runner:latest --build-arg GITLAB_AUTH_TOKEN="${process.env.GITLAB_AUTH_TOKEN}" -f containers/production/aws-s3/Dockerfile .`,
            {
                cwd: RootDir,
                env: { ...process.env, NODE_ENV: 'production' },
            }
        );
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
}

run();
