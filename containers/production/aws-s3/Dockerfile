FROM alpine:latest as build

RUN apk add nodejs npm

ENV NODE_ENV=production

COPY ./dist/src /app
COPY ./package.json /app/package.json
COPY ./.npmrc /app/.npmrc

WORKDIR /app

ARG GITLAB_AUTH_TOKEN
ENV GITLAB_AUTH_TOKEN=${GITLAB_AUTH_TOKEN}
RUN npm install --force
RUN npm install aws-sdk

RUN rm -f /app/.npmrc

FROM mongo:4.4.11-focal

COPY --from=build /app /app

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update
RUN apt install curl
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt install -y nodejs
RUN node -v

ENV MONGODUMP_PATH=/usr/bin/mongodump

WORKDIR /app

RUN mongodump --version

CMD [ "node", "run.js" ]
