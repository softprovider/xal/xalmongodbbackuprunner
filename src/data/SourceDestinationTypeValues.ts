//= Structures & Data
// Own
import { SourceDestinationType } from "./SourceDestinationType";

export const SourceDestinationTypeValues = Object.values(SourceDestinationType);
