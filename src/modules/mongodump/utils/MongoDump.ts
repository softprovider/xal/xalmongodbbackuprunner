//= Functions & Modules
// Others
import { exec } from 'child_process';

const MONGODUMP_PATH_ARG = '$MONGODUMPPATH';
const OUTPUT_ARG = '$1';
const MONGOURI_ARG = '$2';
const MONGODUMP_COMMAND = `${MONGODUMP_PATH_ARG} --gzip -o ${OUTPUT_ARG} --uri "${MONGOURI_ARG}"`;

export default class MongoDump {
    private mongodumpPath: string;

    setMongoDumpPath(mongodumpPath: string) {
        this.mongodumpPath = mongodumpPath;
    }

    async execute(output: string, mongoURI: string): Promise<any> {
        const command = MONGODUMP_COMMAND.replace(MONGODUMP_PATH_ARG, this.mongodumpPath)
            .replace(OUTPUT_ARG, output)
            .replace(MONGOURI_ARG, mongoURI);

        if (process.env.NODE_ENV != 'production') console.log('MongoDump:execute:command', command);

        const childProcess = exec(command, {});

        return new Promise((resolve, reject) => {
            childProcess.on('error', (error) => {
                console.error(`[error]${error.toString()}`);
            });

            childProcess.on('exit', (code, signal) => {
                if (code !== 0) {
                    console.error(`[exit]Done, but with errors: code ${code}, signal ${signal}`);
                    reject({ code, signal });
                } else {
                    console.log('[buildPackage:tsc:exit]Done with code 0');
                    resolve(true);
                }
            });

            childProcess.stdout.on('data', (data) => {
                process.stdout.write(`${data.toString()}`);
            });

            childProcess.stderr.on('data', (data) => {
                process.stderr.write(`${data.toString()}`);
            });
        });
    }
}
