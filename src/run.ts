//= Functions & Modules
// Own
import Runner from './utils/Runner';
// Others
import jsYaml from 'js-yaml';
import { readFile } from 'fs/promises';

//= Structures & Data
// Own
import { SourceDestinationType } from './data/SourceDestinationType';
import { SourceDestinationTypeValues } from './data/SourceDestinationTypeValues';
import { AWSS3SourceDestination } from './source_destinations/aws-s3/utils/AWSS3SourceDestination';

const ENV_CONFIG_PATH = 'CONFIG_PATH';
const ENV_MONGODUMP_PATH = 'MONGODUMP_PATH';

type Config = {
    dbURI: string;
    localDirPath: string;
    destinationType: SourceDestinationType;
    destinationDirPath: string;
    destinationDirName: string;
    mongoDumpPath?: string;
};

type AWSS3_Config = Config & {
    s3_endpoint: string;
    s3_bucket: string;
    s3_key: string;
    s3_secret: string;
};

async function run() {
    if (!process.env[ENV_CONFIG_PATH]) {
        console.error(`Env variable "${ENV_CONFIG_PATH}" is missing`);
        process.exit(1);
    }

    const config: Config = <Config>jsYaml.load(await readFile(process.env[ENV_CONFIG_PATH], 'utf8'));

    if (!config.dbURI) {
        console.error(`Config variable "dbURI" is missing`);
        process.exit(1);
    }

    if (!config.localDirPath) {
        console.error(`Config variable "localDirPath" is missing`);
        process.exit(1);
    }

    if (!config.destinationType) {
        console.error(`Config variable "destinationType" is missing`);
        process.exit(1);
    }

    if (!SourceDestinationTypeValues.includes(config.destinationType)) {
        console.error(`Config variable "destinationType" is wrong`);
        process.exit(1);
    }

    if (!config.destinationDirPath) {
        console.error(`Config variable "destinationDirPath" is missing`);
        process.exit(1);
    }

    if (!config.mongoDumpPath && !process.env[ENV_MONGODUMP_PATH]) {
        console.error(`Config variable "mongoDumpPath" or environment variable "${ENV_MONGODUMP_PATH}" is missing`);
        process.exit(1);
    }

    const runner = new Runner();
    runner.setDatabaseURI(config.dbURI);
    runner.setLocalDirBackupPath(config.localDirPath);
    runner.setDestinationDirBackupPath(config.destinationDirPath);
    runner.setDestinationDirNameFormat(config.destinationDirName);
    runner.setMongoDumpPath(config.mongoDumpPath || process.env[ENV_MONGODUMP_PATH]);

    if (config.destinationType == SourceDestinationType.AWS_S3) {
        const sourceDestination = new AWSS3SourceDestination();
        sourceDestination.setEndpoint((<AWSS3_Config>config).s3_endpoint);
        sourceDestination.setCredentials((<AWSS3_Config>config).s3_key, (<AWSS3_Config>config).s3_secret);
        sourceDestination.setBucket((<AWSS3_Config>config).s3_bucket);
        sourceDestination.createConnection();

        runner.setSourceDestination(sourceDestination);
    }

    try {
        await runner.run();
        process.exit(0);
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
}

run();
