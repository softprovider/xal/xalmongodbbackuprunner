//= Functions & Modules
// Own
import SourceDestinationBase from '../../base';
// Others
import aws from "aws-sdk";
import { createReadStream } from "fs";

export class AWSS3SourceDestination extends SourceDestinationBase {
    private s3: aws.S3;
    private bucket: string;
    private endpoint: string;
    private credentials: { key: string, secret: string };

    constructor() {
        super();
    }

    public setEndpoint(endpoint: string) {
        this.endpoint = endpoint;
    }

    public setCredentials(key: string, secret: string) {
        this.credentials = { key, secret };
    }

    public setBucket(bucket: string) {
        this.bucket = bucket;
    }

    public createConnection() {
        this.s3 = new aws.S3({ endpoint: new aws.Endpoint(this.endpoint), credentials: new aws.Credentials({ accessKeyId: this.credentials.key, secretAccessKey: this.credentials.secret }) });
    }

    public uploadFile(destinationFilePath: string, localFilePath: string, acl: string = "private") {
        return this.s3.putObject({
            Bucket: this.bucket,
            Key: destinationFilePath,
            Body: createReadStream(localFilePath),
            ACL: acl
        }).promise();
    }
}
