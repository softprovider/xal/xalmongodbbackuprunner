export abstract class SourceDestinationBase {
    public abstract uploadFile(destinationFilePath: string, localFilePath: string, acl?: string): Promise<any>;
}
