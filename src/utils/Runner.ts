//= Functions & Modules
// Own
import MongoDump from '../modules/mongodump';
import SourceDestinationBase from '../source_destinations/base';
// Others
import path from 'path';
import fs from 'fs/promises';

export default class Runner {
    private dbURI: string;
    private localBackupDirPath: string;
    private destinationBackupPath: string;
    private sourceDestination: SourceDestinationBase;
    private destinationDirNameFormat: string;
    private mongoDump: MongoDump;

    constructor() {
        this.mongoDump = new MongoDump();
    }

    private async createLocalBackup(localDirName: string) {
        await this.mongoDump.execute(path.join(this.localBackupDirPath, localDirName), this.dbURI);
    }

    private async uploadBackup(destinationDirName: string, localDirName: string) {
        const baseLocalPathName = path.join(this.localBackupDirPath, localDirName);
        const baseDestinationPathName = path.join(this.destinationBackupPath, destinationDirName);

        const items: { local: string; dest: string }[] = [{ local: baseLocalPathName, dest: baseDestinationPathName }];

        while (items.length) {
            const item = items[0];

            const fileStats = await fs.lstat(item.local);

            if (fileStats.isDirectory()) {
                const files = await fs.readdir(item.local);

                for (const file of files) {
                    items.push({ local: path.join(item.local, file), dest: path.join(item.dest, file) });
                }
            } else {
                if (process.env.NODE_ENV != 'production') console.log('Runner:uploadBackup', item.dest, item.local);
                await this.sourceDestination.uploadFile(item.dest, item.local);
            }

            items.splice(0, 1);
        }
    }

    private calculateDestinationFile(): string {
        let replacers: { [key: string]: string } = {};

        if (
            this.destinationDirNameFormat.includes('%Y') ||
            this.destinationDirNameFormat.includes('%M') ||
            this.destinationDirNameFormat.includes('%D') ||
            this.destinationDirNameFormat.includes('%H') ||
            this.destinationDirNameFormat.includes('%m')
        ) {
            const date = new Date();

            const month = date.getMonth() + 1;
            const day = date.getDate();
            const hours = date.getHours();
            const minutes = date.getMinutes();

            replacers.date_Y = date.getFullYear().toString();
            replacers.date_M = `${month < 10 ? '0' : ''}${month.toString()}`;
            replacers.date_D = `${day < 10 ? '0' : ''}${day.toString()}`;
            replacers.date_H = `${hours < 10 ? '0' : ''}${hours.toString()}`;
            replacers.date_m = `${minutes < 10 ? '0' : ''}${minutes.toString()}`;
        }

        return this.destinationDirNameFormat
            .replace('%Y', replacers.date_Y)
            .replace('%M', replacers.date_M)
            .replace('%D', replacers.date_D)
            .replace('%H', replacers.date_H)
            .replace('%m', replacers.date_m);
    }

    public setDatabaseURI(dbURI: string) {
        this.dbURI = dbURI;
    }

    public setLocalDirBackupPath(localBackupDirPath: string) {
        this.localBackupDirPath = localBackupDirPath;
    }

    public setDestinationDirBackupPath(destinationBackupPath: string) {
        this.destinationBackupPath = destinationBackupPath;
    }

    public setSourceDestination(sourceDestination: SourceDestinationBase) {
        this.sourceDestination = sourceDestination;
    }

    public setDestinationDirNameFormat(destinationDirNameFormat: string) {
        this.destinationDirNameFormat = destinationDirNameFormat;
    }

    public setMongoDumpPath(mongoDumpPath: string) {
        this.mongoDump.setMongoDumpPath(mongoDumpPath);
    }

    public async run() {
        let localDirName = `backup`;
        let destinationDirName = this.calculateDestinationFile();

        await this.createLocalBackup(localDirName);
        await this.uploadBackup(destinationDirName, localDirName);
    }
}
